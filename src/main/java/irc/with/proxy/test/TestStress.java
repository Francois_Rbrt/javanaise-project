package irc.with.proxy.test;

import api.proxy.ProxyJvnObject;
import irc.with.proxy.Sentence;
import irc.with.proxy.SentenceImpl;
import irc.with.proxy.test.multithread.StressThread;

import java.util.ArrayList;
import java.util.List;

public class TestStress {
    public static void main(String[] args) {
        if (args.length != 2) {
            System.out.println("Error in args");
            System.exit(0);
        }

        int nb_req_per_client = Integer.valueOf(args[0]);
        int nb_shared_obj = Integer.valueOf(args[1]);

        List<Sentence> s = new ArrayList<>();
        for(int i=0;i<nb_shared_obj;i++){
            Sentence sentence = (Sentence) ProxyJvnObject.newInstance(new SentenceImpl(),"sentence"+i);
            s.add(sentence);
        }
        Thread t=new Thread(new StressThread(nb_req_per_client,nb_shared_obj,0,s));
        t.start();
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
