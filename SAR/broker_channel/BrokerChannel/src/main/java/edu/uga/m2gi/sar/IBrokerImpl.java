package main.java.edu.uga.m2gi.sar;

import java.util.HashMap;

public class IBrokerImpl implements IBroker {
    /* Ibroker :
     Hashmap de  ( paire de Ichannels ) / port associé
     on associe 2 Ichannel pour un port : dans chaque paire il y a les deux IChannels de la connexion : le "client" et le "serveur"
     Au lancement d'une communication, le "premier arrivant" est mit en attente jusqu'a la création du second IChannel.
     Pour leur permettre de communiquer ensemble, le IBroker définit chaque "autre extrémité" de la connexion pour chaque IChannel
     La hashmap représente l'ensemble des connexions soit : En cours de connexion / En cours d'echange / En cours de fermeture (il n'y a plus qu'un membre de connecté)
     - Essayer de se connecter sur un port déjà utilisé relevera une exception RunTime ( -> ajouter doc tmtc )
     - Une absence de paire pour un port donné signifie qu'il n'y a pas de connexion.
     - En cours de connexion : il n'y a qu'un seul IChannel présent dans la hashmap pour un port donné
     - En cours d'echange : Les deux IChannel sont dans la hashmap pour un port donné et sont tous les deux ouvert
     - En cours de fermeture : Les deux IChannels sont dasn la hashmap pour un port donné, un des deux est fermé
     */
    class CoupleClientServer {
        public IChannelImpl client;
        public IChannelImpl serveur;

        public CoupleClientServer() {
            client = null;
            serveur = null;
        }
    }

    public HashMap<Integer, CoupleClientServer> mapIdCouple;
    private final int capacity;

    public IBrokerImpl(int capacity) {
        mapIdCouple = new HashMap<Integer, CoupleClientServer>();
        this.capacity = capacity;
    }

    public IChannel accept(int port) {
        CoupleClientServer couple = mapIdCouple.get(port);
        if (couple != null && couple.serveur != null){
            if(!couple.client.isClosed) {
                throw new RuntimeException("Port déjà utilisé par une autre connexion");
            }
            //If connexion closed, we can reuse the port
            mapIdCouple.remove(port);
            couple = null;
        }  
        if (couple == null) {
            CoupleClientServer new_couple = new CoupleClientServer();
            IChannelImpl serv = new IChannelImpl(capacity);
            new_couple.serveur = serv;
            mapIdCouple.put(port, new_couple);
            synchronized(new_couple){
                try {
                    new_couple.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            serv.remoteChannel = new_couple.client;
            return serv;
        }
        IChannelImpl serv = new IChannelImpl(capacity);
        couple.serveur = serv;
        serv.remoteChannel = couple.client;
        synchronized(couple){
            couple.notify();
        }
        return serv;
    }

    public IChannel connect(int port) {
        CoupleClientServer couple = mapIdCouple.get(port);
        if (couple != null && couple.client != null)
            throw new RuntimeException("Port déjà utilisé par une autre connexion");
        if (couple == null) {
            CoupleClientServer new_couple = new CoupleClientServer();
            IChannelImpl client = new IChannelImpl(capacity);
            new_couple.client = client;
            mapIdCouple.put(port, new_couple);
            synchronized(new_couple){
                try {
                    new_couple.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            client.remoteChannel = new_couple.serveur;
            return client;
        }
        IChannelImpl client = new IChannelImpl(capacity);
        couple.client = client;
        client.remoteChannel = couple.serveur;
        synchronized(couple){
            couple.notify();
        }
        return client;
    }
}
