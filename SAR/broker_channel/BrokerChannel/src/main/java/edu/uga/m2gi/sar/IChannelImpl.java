package main.java.edu.uga.m2gi.sar;


public class IChannelImpl implements IChannel {
    /*
     * Ichannel: boolean closed : Représente l'état ouvert ou fermé de cette
     * extremité de la connexion CircularBuffer readingBuffer : Représente le buffer
     * de lecture de cette extremité de connexion, Ichannel remoteChannel :
     * reference sur le channel distant, on écrit dans son readingBuffer au moment
     * de l'ecriture
     */
    public boolean isClosed;
    public CircularBuffer readingBuffer;
    public IChannelImpl remoteChannel;

    public IChannelImpl(int capacity) {
        isClosed = false;
        readingBuffer = new ArrayCircularBuffer(capacity);
        remoteChannel = null;
    }

    public boolean empty() {
        return readingBuffer.empty();
    }

    public boolean full() {
        return remoteChannel.readingBuffer.full();
    }

    public int read(byte[] buffer, int offset, int count) {
        if (isClosed) {
            return -1;
        }
        int compt;
        synchronized(readingBuffer){
            compt = 0;
            //It blocks if there are no bytes to read, that is, the method "empty()" would return true -> will be notified when the other part
            if (readingBuffer.empty()) { 
                try {
                    readingBuffer.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(remoteChannel.isClosed)
                    return -1;
            //At most, count bytes will be read
            while (!readingBuffer.empty() && compt < count) {
                
                //The read bytes are stored in the given buffer, starting at the given offset.
                byte read_byte = readingBuffer.pull();
                //System.out.print("received : " + read_byte+" / ");
                buffer[offset+compt]=read_byte;
                compt++;
            }
            System.out.println();
            //the readingbuffer is no longer full if it was : notify the other side he can push something
            readingBuffer.notifyAll();
        }
        return compt;
    }

    public int write(byte[] buffer, int offset, int count) {
        int compt;
        synchronized(remoteChannel.readingBuffer) {
            compt = 0;
            //This method attempts to write bytes to this channel, if there is room to do so; it blocks otherwise.
            //It blocks if there are no place to write any byte, that is, the method "full()" would return true -> will be notified when the other part
            if (remoteChannel.readingBuffer.full()) { 
                try {
                    remoteChannel.readingBuffer.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if( isClosed || remoteChannel.isClosed ){
                return -1;
            }
            while (!remoteChannel.readingBuffer.full() && compt < count) {
                //The bytes are copied from the given buffer, starting at the given offset. At most, count bytes will be written.
                //System.out.print("sending : " + buffer[compt+offset] +" / ");
                remoteChannel.readingBuffer.push(buffer[compt+offset]);
                compt++;  
            }  
            System.out.println();
            //the remote reading buffer is no longer empty : notify the other side it can pull something
            remoteChannel.readingBuffer.notifyAll();       
        }
        return compt;
    }

    public void close() {
        isClosed = true;
        synchronized(remoteChannel.readingBuffer) {
            remoteChannel.readingBuffer.notifyAll();
        }
        synchronized(readingBuffer) {
            readingBuffer.notifyAll();
        }
    }
}
