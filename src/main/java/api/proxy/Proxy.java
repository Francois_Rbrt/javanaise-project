/***
 * JAVANAISE API
 * Provy interface
 * Contact:
 *
 * Authors: ROBERT François, SAULLE Laura
 */

package api.proxy;

import api.jvn.JvnException;

import java.lang.reflect.Method;

public interface Proxy {

    /**
     * Call intercept object method when ProxyJvnObject is used
     * @param proxy object encapsulated with proxy
     * @param method method to call
     * @param args arguments of method
     * @return result of method called
     * @throws JvnException
     * @throws Exception
     */
    Object invoke(Object proxy, Method method, Object[] args) throws JvnException, Exception;
}
