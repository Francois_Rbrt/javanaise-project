package main.java.edu.uga.m2gi.sar;

import java.util.ArrayList;

public class Client implements Runnable{
    IBroker iBroker;
    int port;
    IChannel iChannel;
    int capacity;
    boolean isActiv;
    private ClientComponent graphiqueComponent;

    //Constructeur (broker port)
    public Client(IBroker iBroker, int port, int capacity,ClientComponent c) {
        this.iBroker = iBroker;
        this.port = port;
        this.capacity = capacity;
        this.isActiv=true;
        this.graphiqueComponent=c;
    }

    @Override
    public void run() {
        //Connexion au serveur
        iChannel = iBroker.connect(port);

        // Lance un thread Lecteur qui récupère les infos à afficher
        Thread clientReader = new Thread(new ClientReader(iChannel));
        clientReader.start();

        //Wait until the end of the reader execution
        try {
            clientReader.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /*
    Cette classe représente un thread lecteur côté client, il y en a un pour chaque client lancé depuis l'interface graphique.
    Un thread lecteur va lire en continue sur le buffer de réception du Ichannel de sa connexion.
    Quand il reçoit un message du serveur, le message est décodé et active la mise à jour de l'interface graphique
     */
    class ClientReader implements Runnable {
        private IChannel channel;

        public ClientReader(IChannel ch){
            this.channel=ch;
        }

        @Override
        public void run() {
            Message message = new MessageImpl();
            while (getIsActiv()) {
                //on lit un message envoyé par le serveur, appel potentiellement bloquant si pas de message
                message.receiveMessage(iChannel);
                //on a la garantie ici que s contient un message complet qui correspond au format de notre protocole d'échange
                String[] s = message.decodeMessage();
                //on fait les appels a la vue correspondant au message décodé : ce traitement dépend du format de notre protocole d'échange
                MessageReceivedEvent evt=new MessageReceivedEvent(graphiqueComponent,s);
                graphiqueComponent.evtq.postEvent(evt);
            }
            //TODO : appel sur la vue pour dire que le client est arrêté ?
        }
    }



    public synchronized boolean getIsActiv(){
        return this.isActiv;
    }

    public synchronized void stopClient(){
        this.isActiv=false;
    }
}
