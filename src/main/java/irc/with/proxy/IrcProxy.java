package irc.with.proxy;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IrcProxy extends JPanel{
    public TextArea text;
    public TextField data;
    Sentence sentence;

    /**
     * IRC Constructor
     * Create a component wich can be a par of a JFrame
     @param jo the JVN object representing the Chat
     **/
    public IrcProxy(Sentence jo) {
        sentence = jo;
        text = new TextArea(10,60);
        text.setEditable(false);
        text.setForeground(Color.red);
        text.setBackground(Color.black);
        add(text);
        data = new TextField(40);
        add(data);
        Button read_button = new Button("read");
        read_button.addActionListener(new readListener(this));
        add(read_button);
        Button write_button = new Button("write");
        write_button.addActionListener(new writeListener(this));
        add(write_button);
    }


    /**
     * Internal class to manage user events (read) on the CHAT application
     **/
    class readListener implements ActionListener {
        IrcProxy irc;

        public readListener (IrcProxy i) {
            irc = i;
        }

        /**
         * Management of user events
         **/
        public void actionPerformed (ActionEvent e) {
            // invoke the method
            String s = irc.sentence.read();

            // display the read value
            irc.data.setText(s);
            irc.text.append(s+"\n");
        }
    }


    /**
     * Internal class to manage user events (write) on the CHAT application
     **/
    class writeListener implements ActionListener {
        IrcProxy irc;

        public writeListener (IrcProxy i) {
            irc = i;
        }

        /**
         * Management of user events
         **/
        public void actionPerformed (ActionEvent e) {
            // get the value to be written from the buffer
            String s = irc.data.getText();

            // invoke the method
            irc.sentence.write(s);
        }
    }
}


