package main.java.edu.uga.m2gi.sar;

import java.util.ArrayList;
import java.util.List;

/*
    Cette classe représente la classe principale du serveur pour permettre la connexion entre plusieurs client.
    Dans un programme "principal" il suffit de créer une instance de cette classe et de lancer un thread prennant cette instance en paramètre.
    Pour créer une instance de cette classe il faut au préalable créer une instance de broker dans le programme principal,
     ainsi qu'une liste de port sur lesquels les clients vont vouloir se connecter, il faut aussi renseigner la taille des buffers de réception du serveur.
     Pour faire évoluer cette implémentation du serveur qui fait communiquer des clients localement :
      on peut remplacer le broker et la liste de port par une Socket Serveur qui accepte les connexions, et créer un lecteur par port ou il y a eut connexion
 */
public class Server implements Runnable{

    private IBrokerImpl broker;
    private List<Integer> port_list;
    private Integer buffer_capacity;
    private List<Thread> reader_list;

    public Server(IBrokerImpl b, List<Integer> port_list,Integer buffer_capacity){
        this.broker=b;
        this.port_list=port_list;
        this.buffer_capacity=buffer_capacity;
        this.reader_list=new ArrayList<Thread>();
    }
    @Override
    public void run() {
        /*
        faire un accept par port : lancer un thread lecteur par nouvelle connexion
         */
        for(Integer port : port_list){
            IChannel current_channel=this.broker.accept(port);
            Thread newReader=new Thread(new ServerReader(current_channel,port,buffer_capacity));
            newReader.start();
            reader_list.add(newReader);
        }
        //Wait for all readers to finish
        for(Thread t : reader_list){
            try {
                t.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    /*
    Cette classe représente un thread lecteur côté serveur, il y en a un pour chaque client connecté au serveur.
    Un thread lecteur va lire en continue sur le buffer de réception du Ichannel de sa connexion.
    Quand le serveur reçoit un message d'un client, on veut que le message soit transmit aux autres client : à la réception d'un message le thread
    lecteur lance un thread écrivain par client à qui il faut transettre le message.
     */
    class ServerReader implements Runnable {
        private IChannel channel;
        private Integer port;
        private Integer capacity;

        public ServerReader(IChannel ch,Integer port,Integer capacity){
            this.channel=ch;
            this.port=port;
            this.capacity=capacity;
        }

         @Override
         public void run() {
            /*
             lit en boucle sur le channel
             récupère les channels qui doivent envoyer le message
             créer un thread ecrivain qui connait le channel sur lequel il faut ecrire + le message a ecrire
             */
             Message message=new MessageImpl();
             while (true){
                 //On lit le maximum d'octets possible, cet appel peut être bloquant s'il n'y a rien à lire
                 message.receiveMessage(channel);
                 //récupère les channels qui doivent envoyer le message
                 ArrayList<IChannelImpl> channel_list=new ArrayList<IChannelImpl>();
                 for(Integer port_to_send : port_list){
                     if(port_to_send!=this.port){
                         channel_list.add(broker.mapIdCouple.get(port_to_send).serveur);
                     }
                 }
                 //créer un thread ecrivain qui connait le channel sur lequel il faut ecrire + le message a ecrire
                 for(IChannel current_channel:channel_list) {
                     new Thread(new ServerWriter(current_channel, message)).start();
                 }
             }
         }
     }

    /*
        Cette classe représente un thread écrivain côté serveur, il y en a un nouveau pour chaque message qu'il faut transmettre à un client.
        Un thread écrivain va écrire le message donnée sur le Ichannel donné.
        Cette action est faite dans un thread car l'appel aux fonctions nécessaire peut être bloquant, et on veut empêcher qu'un lecteur
        soit bloqué.
         */
    class ServerWriter implements Runnable {
        private IChannel channel;
        private Message message;

        public ServerWriter(IChannel ch,Message message){
            this.channel=ch;
            this.message=message;
        }

        @Override
        public void run() {
            message.sendMessage(channel);
        }
    }
}
