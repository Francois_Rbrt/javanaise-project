package irc.with.proxy;

import api.proxy.Log;

public interface Sentence {

    @Log(logtype = Log.LogType.READ)
    public String read();

    @Log(logtype = Log.LogType.WRITE)
    public void write(String text);

}
