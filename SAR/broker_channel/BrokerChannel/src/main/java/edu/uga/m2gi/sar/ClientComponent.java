package main.java.edu.uga.m2gi.sar;

import java.awt.*;

public class ClientComponent extends Component {
        private IBroker broker;
        public TextArea text;
        public EventQueue evtq;
        public Client client;

        public ClientComponent(Frame frame,HackTextSample._KeyListener l,IBroker broker,Integer port,Integer capacity) {
            //Création de la zone de texte
            text = new TextArea("Welcome!");
            text.setPreferredSize(new Dimension(400, 400));
            text.addKeyListener(l);
            frame.add(text);
            this.broker=broker;
            //récuperation de la file d'évenement
            evtq = Toolkit.getDefaultToolkit().getSystemEventQueue();
            enableEvents(0);
            //On lance le thread client
            client=new Client(broker,port,capacity,this);
            Thread clientThread = new Thread(client);
            clientThread.start();
        }

        //Définit comment les évenement de la file seront traités
        public void processEvent(AWTEvent evt) {
           if (evt instanceof MessageReceivedEvent) {
                if(((MessageReceivedEvent)evt).getMessage().length==2){ //ADD
                    text.insert( ((MessageReceivedEvent)evt).getMessage()[1],Integer.parseInt( ((MessageReceivedEvent)evt).getMessage()[0] ));
                }
                else if(((MessageReceivedEvent)evt).getMessage().length==1){ //DEL
                    text.replaceRange("",Integer.parseInt( ((MessageReceivedEvent)evt).getMessage()[0] ),Integer.parseInt( ((MessageReceivedEvent)evt).getMessage()[0] )+1);
                }
            } else
                super.processEvent(evt);
        }

    }
