/***
 * JAVANAISE Implementation
 * JvnCoordImpl class
 * This class implements the Javanaise central coordinator
 * Contact:
 *
 * Authors: ROBERT François, SAULLE Laura
 */

package api.coord;

import api.jvn.*;
import api.server.JvnRemoteServer;
import irc.with.proxy.Sentence;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.rmi.AlreadyBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class JvnCoordImpl
              extends UnicastRemoteObject 
							implements JvnRemoteCoord {
	

  /**
	 * 
	 */
    private static final long serialVersionUID = 1L;
    private static final int MAX_OBJECT = 10;
    private static final int MAX_SERVER = 10;
    private final int NOT_REGiSTERED = 0;
    private final int R = 1;
    private final int W = 2;
    private final int NL = 3;
    private HashMap<Integer, Serializable> jvnObject_id; //to get a reference over all the jvnObject by their id
    private HashMap<String, Integer> jvnObject_name; //to get the id of a jvnObject by one name it is associated with
    private List<JvnRemoteServer> jvnServer_id;
    private int nbOject;
    private int nbServer;
    private int[][] tab;

    private ArrayList<String> journal;
    private boolean afterCrash;

    /**
     * Default constructor
     * @throws RemoteException
     **/
    public JvnCoordImpl() throws RemoteException {
        super();
        Registry registry = LocateRegistry.createRegistry(1099);
        jvnObject_id = new HashMap<>();
        jvnObject_name = new HashMap<>();
        jvnServer_id = new ArrayList<>();
        journal = new ArrayList<>();
        afterCrash=false;
        nbOject = 0;
        nbServer = 0;
        tab = new int[10][10];
        registry.rebind("Coord", this);
        //Init avec le fichier si possible
       try {
            LocalDate date= LocalDate.now();
            FileInputStream fin = new FileInputStream(System.getProperty("user.dir")+"/coord_log_"+date+".txt");
            FileInputStream fin_obj = new FileInputStream(System.getProperty("user.dir")+"/coord_log_obj_"+date+".txt");
            ObjectInputStream oi = new ObjectInputStream(fin_obj);
            int crash=fin.read();
            if (crash==0) //if the coord terminated well the last time we stop
                afterCrash=false;
            else { // else we try to take the full state of the coord before it crashed
                System.out.println("Coord has crashed... reading file ...");
                int card = fin.read();
                System.out.println("card : " + card);
                List<Integer> id_list = new ArrayList(jvnObject_id.keySet());
                List<Serializable> ser_list = new ArrayList(jvnObject_id.values());
                List<String> name_list = new ArrayList(jvnObject_name.keySet());
                for (int i = 0; i < card; i++) {
                    int id=fin.read();
                    System.out.println("id read : "+id);
                    id_list.add(id);
                }
                for (int i = 0; i < card; i++) {
                    System.out.println("obj read : "+i);
                    ser_list.add((Serializable) oi.readObject());
                }
                for (int i = 0; i < card; i++) {
                    String name=Integer.toString(fin.read());
                    System.out.println("name read : "+name);
                    name_list.add(name);
                }
                for (int i = 0; i < id_list.size(); i++) {
                    jvnObject_id.put(id_list.get(i), ser_list.get(i));
                    jvnObject_name.put(name_list.get(i), id_list.get(i));
                }
                nbOject = fin.read();
                System.out.println("nbobject : " + nbOject);
                nbServer = fin.read();
                System.out.println("nbServer " + nbServer);
            }
        } catch (FileNotFoundException e) {
            System.out.println("No log file found : normal initilisation ");
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Private method to print state of coordinator
     * Represent wich server got wich object and its lock value
     * (R : read, W : write, N : not locked, " " : not registered)
     */
    private synchronized void printCoordState() {
        System.out.println();
        for (int i = 0; i < nbOject; i++) {
            System.out.print("\t" + i);
        }
        System.out.println();
        for (int i = 0; i < nbServer; i++) {
            System.out.print("s" +i);
            for (int j = 0; j < nbOject; j++) {
                System.out.print("\t");
                switch (tab[i][j]) {
                    case 0:
                        System.out.print(" ");
                        break;
                    case 1:
                        System.out.print("R");
                        break;
                    case 2:
                        System.out.print("W");
                        break;
                    case 3:
                        System.out.print("N");
                        break;
                }

            }
            System.out.println();
        }
        System.out.println("\n--------------------------------------------------");
    }

    /**
     *  Allocate a NEW JVN object id (usually allocated to a
     *  newly created JVN object)
     *  @return id allocated
     *  @throws RemoteException
     *  @throws JvnException
     **/
    public synchronized int jvnGetObjectId()
            throws JvnException, RemoteException {
        // Choix d'implementation : on a un maximum d'objets pouvant être stockés
        if (nbOject == MAX_OBJECT) {
            throw new JvnException("Too much object shared");
        }
        return nbOject++;
    }

    /**
     * Associate a symbolic name with a JVN object
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @param js  : the remote reference of the JVNServer
     * @throws JvnException
     * @throws RemoteException
     **/
    public synchronized void jvnRegisterObject(String jon, JvnObject jo, JvnRemoteServer js)
            throws JvnException, RemoteException{
        // Un nom n'est associé qu'à un seul objet
        if(jvnObject_name.get(jon) != null){
            throw new JvnException("This object name is already allocated");
        }

        // Enregistrement de l'objet
        Serializable obj = jo.jvnGetSharedObject();
        int id = jo.jvnGetObjectId();
        if(jvnObject_id.containsValue(obj)){    // Simple ajout d'un nom
            jvnObject_name.put(jon,id);
        }
        else{
            jvnObject_name.put(jon,id); // Ajout d'un nouvel objet
            jvnObject_id.put(id,obj);
        }

        // Enregistrement du serveur
        int idServ = jvnServer_id.indexOf(js);
        if (idServ == -1) {
            // Choix d'implémentation : on a un nombre maximum de serveur connecté au coordinateur
            if (nbServer == MAX_SERVER) {
                throw new JvnException("Too much server, cannot connect to coordinator");
            }
            idServ = nbServer;
            jvnServer_id.add(idServ,js);
            nbServer++;
        }
        tab[idServ][id] = NL;
        printCoordState();
    }

    /**
     * Get the reference of a JVN object managed by a given JVN server
     * @param jon : the JVN object name
     * @param js : the remote reference of the JVNServer
     * @return JvnObject or null if it isn't in coordinator
     * @throws RemoteException
     **/
    public JvnObject jvnLookupObject(String jon, JvnRemoteServer js)
            throws RemoteException{
        if (jvnObject_name.get(jon) != null) {
            int id = jvnObject_name.get(jon);
            Serializable obj = jvnObject_id.get(id);

            int idServ = jvnServer_id.indexOf(js);
            if (idServ == -1) {
                idServ = nbServer;
                jvnServer_id.add(idServ,js);
                nbServer++;
            }
            tab[idServ][id] = NL;
            printCoordState();
            return new JvnObjectImpl(id,obj);
        }
        return null;
    }


    /**
     * Private method to get readers of a specific object
     * @param joi id of object
     * @return list of server id of readers
     */
    private List<Integer> getReaders(int joi) {
        List<Integer> readers = new ArrayList<>();
        for (int i = 0; i < nbServer; i++) {
            if (tab[i][joi] == R) {
                readers.add(i);
            }
        }
        return readers;
    }

    /**
     * Private method to get writer of a specific object
     * @param joi id of object
     * @return server id of writer
     */
    private int getWriter(int joi){
        for (int i = 0; i < nbServer; i++) {
            if (tab[i][joi] == W) {
                return i;
            }
        }
        return -1;
    }

    /**
     * Private method to remove writer of a specific object
     * @param joi id of object
     * @param jsId id of server wich want a lock on the object
     * @param option option of invalidation ('R' : invalidate write only, 'W' invalidate all)
     * @return serializable object modificated
     */
    private synchronized Serializable removeWriter(int joi, int jsId, char option) throws JvnException {
        if (!jvnObject_id.containsKey(joi)) {
            throw new JvnException("No object with the id " + joi);
        }

        // l'objet renvoyé sera celui stocké dans le coordinateur si aucun verrou en écriture
        // ou s'il y a un problème de connexion avec le serveur qui possède le verrou
        Serializable obj_state = jvnObject_id.get(joi);

        // Modification du verrou du client qui le possède actuellement, en fonction de l'option
        int jsWriterId = getWriter(joi);
        if(jsWriterId != -1){
            try {
                if (option == 'R' && jsWriterId != jsId) {
                    obj_state = jvnServer_id.get(jsWriterId).jvnInvalidateWriterForReader(joi);
                    tab[jsWriterId][joi] = R;
                }
                else if(option == 'W') {
                    obj_state = jvnServer_id.get(jsWriterId).jvnInvalidateWriter(joi);
                    tab[jsWriterId][joi] = NL;
                }
                jvnObject_id.replace(joi,obj_state);
            }
            // Gestion panne client
            catch (RemoteException | JvnException e) {
                System.out.println("Lost connexion with " + jvnServer_id.get(jsWriterId));
                for (int i = 0; i < nbOject; i++) {
                    tab[jsWriterId][i] = NOT_REGiSTERED;
                }
            }
        }
        return obj_state;
    }

    /**
     * Get a Read lock on a JVN object managed by a given JVN server
     * @param joi : the JVN object identification
     * @param js  : the remote reference of the server
     * @return the current JVN object state
     * @throws JvnException
     * @throws RemoteException
     **/
    public synchronized Serializable jvnLockRead(int joi, JvnRemoteServer js)
            throws JvnException, RemoteException{

        // Suppression d'un éventuel ecrivain sur l'objet
        int jsId = jvnServer_id.indexOf(js);
        Serializable obj_state = removeWriter(joi,jsId,'R');

        // Ajout du verrou en lecture au serveur appelant
        tab[jsId][joi] = R;

        printCoordState();

        return obj_state;
    }


    /**
     * Get a Write lock on a JVN object managed by a given JVN server
     * @param joi : the JVN object identification
     * @param js  : the remote reference of the server
     * @return the current JVN object state
     * @throws RemoteException
     * @throws JvnException
     **/
    public synchronized Serializable jvnLockWrite(int joi, JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException{

        // Suppression d'un éventuel ecrivain sur l'objet
        int jsId = jvnServer_id.indexOf(js);
        Serializable obj_state = removeWriter(joi,jsId,'W');

        // Suppression des éventuels lecteurs sur l'objet
        for (Integer jsReaderId : getReaders(joi)){
            if (jsReaderId != jsId) {
                JvnRemoteServer jsReader = jvnServer_id.get(jsReaderId);
                jsReader.jvnInvalidateReader(joi);
                tab[jsReaderId][joi] = NL;
            }
        }

        // Ajout du verrou en ecriture au serveur appelant
        tab[jsId][joi] = W;

        printCoordState();

        return obj_state;
    }

    /**
     * A JVN server terminates
     * @param js  : the remote reference of the server
     * @throws java.rmi.RemoteException
     * @throws JvnException
     **/
    public synchronized void jvnTerminate(JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException {
        // Suppression de chaque objet détenu par le serveur
        int jsId = jvnServer_id.indexOf(js);
        for (int joi = 0; joi < nbOject; joi++) {
            if (tab[jsId][joi] == W) {
                Serializable obj_state = js.jvnInvalidateWriter(joi);
                jvnObject_id.replace(joi,obj_state);
            }
            tab[jsId][joi] = NOT_REGiSTERED;
        }
        printCoordState();
    }

    /**
     * Remove an object of a remote server
     * @param joi id of the object
     * @param js the remote reference of the server
     * @throws RemoteException
     * @throws JvnException
     */
    @Override
    public void jvnForget(int joi, Serializable obj, JvnRemoteServer js) throws RemoteException, JvnException {
        int jsId = jvnServer_id.indexOf(js);
        if (tab[jsId][joi] == W) {
            jvnObject_id.replace(joi,obj);
        }
        tab[jsId][joi] = NOT_REGiSTERED;
    }
    /**
     * save the state of the coordinator in a file
     */
    public void coord_state_save()throws RemoteException, JvnException {
        //fonction qui enregistre dans un fichier l'état actuel du coordinateur pour reprendre en cas de pane
        // -> la fonction qui est appelé toutes les 30 sec
        ObjectOutputStream oos = null;
        FileOutputStream fout = null;
        FileOutputStream fout_obj = null;
        try{
            LocalDate date= LocalDate.now();
            fout = new FileOutputStream(System.getProperty("user.dir")+"/coord_log_"+date+".txt", false);
            fout_obj = new FileOutputStream(System.getProperty("user.dir")+"/coord_log_obj_"+date+".txt", false);
            oos = new ObjectOutputStream(fout_obj);
            List<Integer> klist = new ArrayList(jvnObject_id.keySet());
            List<Serializable> vList = new ArrayList(jvnObject_id.values());
            List<String> klist2 = new ArrayList(jvnObject_name.keySet());
            //if the coordinator read the file written by this function (not overwritten by the Terminate function), it needs to know it has crashed
            fout.write(1);
            //nb of obj
            fout.write(klist.size());
            //on ecrit tous les etats d'objets
            for(int i=0 ; i<vList.size() ; i++){
                oos.writeObject(vList.get(i));
            }
            //on ecrit tous les id d'objets
            for(int i=0 ; i<klist.size() ; i++){
                fout.write(klist.get(i));
            }
            //on ecrit tous les noms d'objets
            for(int i=0 ; i<klist2.size() ; i++){
                byte[] mybytes = klist2.get(i).getBytes();
                fout.write(mybytes);
            }
            fout.write(nbOject);
            fout.write(nbServer);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if(oos != null){
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /*
    private void add_coord_state_journalisation_message(String s){//TODO : faire appel a cette fonction partout dans le code + rejouer les etapes dans l'init
        //ajoute a la fin du fichier "state coord" l'opération s
        //TODO : ecrire dans le fichier "coord state" le message s à la suite du contenu deja présent
        try {
            LocalDate date= LocalDate.now();
            FileOutputStream fout = new FileOutputStream(System.getProperty("user.dir")+"/coord_log_"+date+".txt", true);
            try  {
                byte[] mybytes = s.getBytes();
                fout.write(mybytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }*/

   public void are_you_alive(List<Integer> lastUse,JvnRemoteServer js)throws RemoteException, JvnException {
       List<Serializable> ser_list=new ArrayList<>();
       //we fill in the serializable list to give the server the actual state of the object in the coordinator
       for(Integer i : lastUse){
           ser_list.add(jvnObject_id.get(i));
       }
        js.clearCache(ser_list);
   }

}

 
