package main.java.edu.uga.m2gi.sar;

import java.awt.*;

public class MessageReceivedEvent extends AWTEvent {
        public static final int MESSAGE_EVENT = AWTEvent.RESERVED_ID_MAX + 5555;
        public static String[] decodedMessage;

        public MessageReceivedEvent(ClientComponent c,String[] _decodedMessage) {
            super(c, MESSAGE_EVENT);
            decodedMessage=_decodedMessage;
        }

        public static synchronized String[] getMessage(){
            return decodedMessage;
        }
    }

