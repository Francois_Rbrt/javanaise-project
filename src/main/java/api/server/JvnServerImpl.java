/***
 * JAVANAISE Implementation
 * JvnServerImpl class
 * Implementation of a Jvn server
 * Contact: 
 *
 * Authors: ROBERT François, SAULLE Laura
 */
package api.server;

import api.coord.JvnRemoteCoord;
import api.jvn.JvnException;
import api.jvn.JvnObject;
import api.jvn.JvnObjectImpl;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.io.*;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.List;


public class JvnServerImpl
              extends UnicastRemoteObject 
							implements JvnLocalServer, JvnRemoteServer {
	
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// A JVN server is managed as a singleton 
	private static JvnServerImpl js = null;
	//A reference to the coordinator
	private JvnRemoteCoord coord = null;
	Data data;

  /**
  * Default constructor
  * @throws RemoteException
  **/
	private JvnServerImpl() throws RemoteException {
		super();
		data = new Data();

		//We use the java RMI to get the reference over the coordinator
		try {
			Registry registry = LocateRegistry.getRegistry();
			coord = (JvnRemoteCoord) registry.lookup("Coord");
		}
		catch (NotBoundException e){
			System.out.println("Coordinator is not launched");
		}
		catch(RemoteException e){
			System.out.println("Cannot connect to the coordinator for some reasons ...");
			System.out.println(e);
			//TODO : if remote communication with the registry failed; if exception is a ServerException containing an AccessException, then the registry denies the caller access to perform this operation
		}
	}
	
  /**
    * Static method allowing an application to get a reference to 
    * a JVN server instance
   * @return JvnServerImpl unique
    **/
	public static JvnServerImpl jvnGetServer() {
		if (js == null){
			try {
				js = new JvnServerImpl();
			} catch (Exception e) {
				return null;
			}
		}
		return js;
	}
	
	/**
	* The JVN service is not used anymore
	* @throws JvnException
	**/
	public  void jvnTerminate() throws api.jvn.JvnException {
		try {
			coord.jvnTerminate(this);
		} catch (RemoteException e) {
			try {
				remoteExceptionHandler();
			} catch (RemoteException remoteException) {
				remoteException.printStackTrace();
			}
			throw new JvnException(e.getMessage());
		}
	}

	@Override
	public void jvnForget(int joi, Serializable obj) throws JvnException {
		try {
			coord.jvnForget(joi,obj,this);
		} catch (RemoteException e) {
			try {
				remoteExceptionHandler();
			} catch (RemoteException remoteException) {
				remoteException.printStackTrace();
			}
			throw new JvnException(e.getMessage());
		}
	}

	/**
	* creation of a JVN object
	* @param o : the JVN object state
	* @throws JvnException
	 * @throws RemoteException
	**/
	public JvnObject jvnCreateObject(Serializable o) throws api.jvn.JvnException, RemoteException {
		int id;
		try {
			id = coord.jvnGetObjectId();
		} catch (RemoteException e) {
			try {
				remoteExceptionHandler();
			} catch (RemoteException remoteException) {
				remoteException.printStackTrace();
			}
			throw new JvnException(e.getMessage());
		}
		JvnObjectImpl jo = new JvnObjectImpl(id,o);
		//We reference this new local object by its id
		data.put(jo.jvnGetObjectId(),jo);
		return jo;
	}

	/**
	*  Associate a symbolic name with a JVN object
	* @param jon : the JVN object name
	* @param jo : the JVN object 
	* @throws JvnException
	**/
	public void jvnRegisterObject(String jon, JvnObject jo) throws api.jvn.JvnException {
		try {
			//reference the object into the coordinator
			coord.jvnRegisterObject(jon,jo,this);
			//Reference the new object into the local server
			data.put(jon,jo.jvnGetObjectId());
		} catch (Exception e) {
			try {
				remoteExceptionHandler();
			} catch (RemoteException remoteException) {
				remoteException.printStackTrace();
			}
			throw new JvnException(e.getMessage());
		}
	}
	
	/**
	* Provide the reference of a JVN object beeing given its symbolic name
	* @param jon : the JVN object name
	* @return the JVN object 
	* @throws JvnException
	**/
	public JvnObject jvnLookupObject(String jon) throws api.jvn.JvnException {
		if(data.contains(jon)) {
			int id = data.getId(jon);
			return data.getObject(id);
		}
		try {
			JvnObject o = coord.jvnLookupObject(jon,this);
			if (o != null) {
				data.put(jon,o.jvnGetObjectId(),o);
			}
			return o;
		} catch (RemoteException e) {
			try {
				remoteExceptionHandler();
			} catch (RemoteException remoteException) {
				remoteException.printStackTrace();
			}
			throw new JvnException(e.getMessage());
		}
	}

	/**
	 * Inform data of using object
	 * @param joi id of the object
	 * @throws JvnException
	 */
	public int updateData(int joi) throws JvnException {
		return data.update(joi);
	}
	
	/**
	* Get a Read lock on a JVN object 
	* @param joi : the JVN object identification
	* @return the current JVN object state
	* @throws  JvnException
	**/
   public Serializable jvnLockRead(int joi)
	 throws JvnException {
	   try {
	   	return this.coord.jvnLockRead(joi, this);
	   } catch (RemoteException e) {
		   try {
			   remoteExceptionHandler();
		   } catch (RemoteException remoteException) {
			   remoteException.printStackTrace();
		   }
		   throw new JvnException(e.getMessage());
	   }
	}	
	/**
	* Get a Write lock on a JVN object 
	* @param joi : the JVN object identification
	* @return the current JVN object state
	* @throws JvnException
	**/
   public Serializable jvnLockWrite(int joi)
	 throws JvnException {
	   try {
		   return this.coord.jvnLockWrite(joi, this);
	   } catch (RemoteException e) {
		   try {
			   remoteExceptionHandler();
		   } catch (RemoteException remoteException) {
			   remoteException.printStackTrace();
		   }
		   throw new JvnException(e.getMessage());
	   }
	}	

	
  /**
	* Invalidate the Read lock of the JVN object identified by id 
	* called by the JvnCoord
	* @param joi : the JVN object id
	* @throws RemoteException
    * @throws JvnException
	**/
  public void jvnInvalidateReader(int joi)
	throws RemoteException, api.jvn.JvnException {
	  	JvnObject o = data.getObject(joi);
	 	o.jvnInvalidateReader();
	}
	    
	/**
	* Invalidate the Write lock of the JVN object identified by id 
	* @param joi : the JVN object id
	* @return the current JVN object state
	* @throws RemoteException
	 * @throws JvnException
	**/
  public Serializable jvnInvalidateWriter(int joi)
	throws RemoteException, api.jvn.JvnException {
	  JvnObject o = data.getObject(joi);
	  return o.jvnInvalidateWriter();
	}
	
	/**
	* Reduce the Write lock of the JVN object identified by id 
	* @param joi : the JVN object id
	* @return the current JVN object state
	* @throws RemoteException
	 * @throws JvnException
	**/
   public Serializable jvnInvalidateWriterForReader(int joi)
	 throws RemoteException, api.jvn.JvnException {
	   JvnObject o = data.getObject(joi);
	   return o.jvnInvalidateWriterForReader();
	 }
	/**
	 * This function is called every time there is a remote exception in order to handle a coordinator crash
	 **/
	 public void remoteExceptionHandler()
			 throws java.rmi.RemoteException{
	 	int nb_try=0;
	 	while(nb_try<5) {
			try {
				Thread.sleep(10000); //wait 10sec before the next try
				nb_try++;
				this.coord.are_you_alive(this.data.getLastUse(),this);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (RemoteException e) {
				e.printStackTrace();
			} catch (JvnException e) {
				e.printStackTrace();
			}
		}
	 }

	/**
	 * this method is called when the coordinator restart after a crash, to update all the servers state to maintain coherence
	 * @param ser_list : the list of state of objects the server will have to put up to date in his cache
	 * @throws java.rmi.RemoteException
	 **/
	public void clearCache(List<Serializable> ser_list)
			throws java.rmi.RemoteException{
			this.data.clearCache(ser_list);
	}

}

 
