package irc.with.proxy.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Test {
    public static void main (String[] args) throws IOException {
        if (args.length != 3) {
            System.out.println("Usage : ./test <nb_process> <nb_req_client> <nb_shared_object>");
            System.exit(0);
        }

        int nbProc = Integer.valueOf(args[0]);
        int nbReqClient = Integer.valueOf(args[1]);
        int nbSharedObject = Integer.valueOf(args[2]);

        List<Process> app = new ArrayList<>();

        String cmd;
        Scanner scanner = new Scanner(System.in);

        String cmdProc = "java -jar target/launcher-app-test-jar-with-dependencies.jar "
                + nbReqClient + " " + nbSharedObject;

        System.out.println(cmdProc);

        for (int i = 0; i < nbProc; i++) {
            app.add(Runtime.getRuntime().exec
                    (cmdProc));
        }

        do{
            cmd = scanner.nextLine();
        }while(!cmd.equals("quit"));

        for (Process process : app) process.destroy();
    }
}
