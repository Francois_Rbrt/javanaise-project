package irc.with.proxy.test.multithread;

import api.jvn.JvnException;
import api.server.JvnServerImpl;
import irc.with.proxy.Sentence;

import java.util.List;
import java.util.Random;

public class StressThread implements Runnable {
    private List<Sentence> s;
    private int nb_req;
    private int nb_obj;
    private JvnServerImpl js;
    private int id;
    private Random r;

    public StressThread(int _nb_req,int _nb_obj,int _id, List<Sentence> s){
        this.s = s;
        this.nb_req = _nb_req;
        this.nb_obj = _nb_obj;
        this.id = _id;
        this.js = JvnServerImpl.jvnGetServer();
        r = new Random();
    }
    @Override
    public void run() {
        int req_performed=0;
        int nb_fail=0;
        while(req_performed<this.nb_req){
            //Generation of the random query :
            int action = r.nextInt(2); // 0 : Read / 1 : Write
            //int action = 1;

            int shared_obj_choosen = r.nextInt(nb_obj);
            //System.out.println("[INFO] client [ "+id+" ] "+action + " / "+shared_obj_choosen);

            //We take the shared object reference
            String name = "sentence" + shared_obj_choosen;

            //We can perform an action on the object
            try {
                if (action == 0) {
                    System.out.println("[READ] client [ " + id + " ] object [ "+name+" ] : " + s.get(shared_obj_choosen).read());
                } else {
                    s.get(shared_obj_choosen).write(Integer.toString(this.id));
                    System.out.println("[WRITE] client [ " + id + " ] object [ "+name+" ] : " + id);
                }
                req_performed++;
                nb_fail=0;
            }
            catch (Exception e){
                System.out.println(e);
                nb_fail++;
                if(nb_fail>=3){
                    try {
                        js.jvnTerminate();
                    } catch (JvnException e2) {
                        e2.printStackTrace();
                    }
                    System.out.println("[END] client [ "+id+" ] has crashed...");
                    return;
                }
            }

        }
        //the client has finished its job
        try {
            js.jvnTerminate();
        } catch (JvnException e) {
            e.printStackTrace();
        }
        System.out.println("[END] client [ "+id+" ] has finished...");
        System.exit(0);
    }
}
