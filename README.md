# Javanaise Project

## Description
The Javanaise API is a java manager of shared object, based on RMI service.

## Installation
 * Clone repo to have sources.
 * Download release to launch coordinator, app and test as explained below.

## Usage 

### Coordinator start
It's necessary to launch coordinator before launching your app :
```bash
    java -jar jvn-launcher-coord-jar-with-dependencies.jar
```

tap ```quit``` to close launcher

### User code
* Create serializable class for shared object. Its interface must have annotation on each method : 


```java
@Log(logtype = Log.LogType.READ)
```
or
```java
 @Log(logtype = Log.LogType.WRITE)
```

 
* Get a JvnServer to communicate :
```java
    Jvnerver jvnServer = JvnServerImpl.jvnGetServer();
``` 

* Create my shared object : 
```java
    MyObjectInterface myObj = (MyObjectInterface) ProxyJvnObject.newInstance(new MyObjectImpl(),<nameOfObject>);
```

At this time, you can use your object as well. When you want to quit application, you have to terminate JvnServer :
```java
    js.jvnTerminate();
```

### Demo
You can test API with an app given :
```bash
    java -jar app-demo-jar-with-dependencies.jar
```

### Test
A test program wich launch one process and try to read and write on somes variables it created :

```bash
    java -jar launcher-app-test-jar-with-dependencies.jar <nb_req> <nb_var>
```


[Javadoc](https://francois_rbrt.gitlab.io/-/javanaise-project/-/jobs/797543986/artifacts/public/index.html)