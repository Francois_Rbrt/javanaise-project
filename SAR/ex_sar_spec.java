package edu.uga.m2gi.sar;

/** *  circular buffer is a particular type of a communication channel: a buffer of a given fixed capacity that can be used to push and pull bytes. */
public abstract class CircularBuffer {
    /**   
    *    Return if the buffer is full or not
    *    @return true if the buffer is full, false either  
    */
    public abstract boolean full();

    /** 
     *    Return if the buffer is empty or not
     *    @return true if the buffer is empty, false either  
    */
    public abstract boolean empty();

    /**   
    *    Push an element at the end of the buffer  (may throw error if the buffer is full ..)
    *    @param byte b : the byte we want to push in the buffer      
    */
    public abstract void push(byte b);

    /**
     *    Get the first byte available on the circular buffer (may throw error if the buffer is empty ..) 
     *    @return the byte at the beggining of the buffer
     **/
    public abstract byte pull();
    
}