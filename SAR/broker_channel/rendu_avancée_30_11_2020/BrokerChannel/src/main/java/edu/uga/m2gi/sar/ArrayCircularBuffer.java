package edu.uga.m2gi.sar;

public class ArrayCircularBuffer extends CircularBuffer{

	private byte buffer[];
	private int indexRead;
	private int indexWrite;
	private int capacity;
	private boolean isFull =false;

	//Default constructor : the default capacity is 128 bytes
	public ArrayCircularBuffer() {
		indexRead=0;
		indexWrite=0;
		capacity=128;
		buffer=new byte[capacity];
	}

	//the parameter : int cap, is the capacity of the created buffer in bytes
	public ArrayCircularBuffer(int cap) {
		indexRead=0;
		indexWrite=0;
		capacity=cap;
		buffer=new byte[capacity+1];
	}

	@Override
	public synchronized boolean full() {
		return isFull;
	}

	@Override
	public synchronized boolean empty() {
		return indexWrite == indexRead && !isFull;
	}

	@Override
	public synchronized void push(byte b) throws IllegalStateException{
		if(full())
			throw new IllegalStateException("The buffer is full");
		else {
			buffer[indexWrite]=b;
			indexWrite = (indexWrite+1)%capacity;
			if(indexWrite == indexRead)
				isFull=true;
		}
	}

	@Override
	public synchronized byte pull() throws IllegalStateException{
		if(empty())
			throw new IllegalStateException("The buffer is empty");
		else {
			isFull=false;
			byte res=buffer[indexRead];
			indexRead=(indexRead+1)%capacity;
			return res;
		}
	}

}
