package main.java.edu.uga.m2gi.sar;

public interface Message {
    /***
     * Méthode qui permet de créer un message selon le format suivant :
     * Si c'est un ajout de caractere : <offset_du_nouveau_char>;<char>
     * Si c'est une suppression : <offset_à_supprimer>
     * Le message est encodé en UTF-8
     * @param mode mode du message à envoyer. ADD pour ajouter un caractere à droite de l'offset,
     *            DEL pour en supprimer un à l'offset indiqué.
     * @param offset offset auquel faire la transformation.
     * @param c caractere à ajouter. Si le mode est DEL, il ne sera pas pris en compte.
     */
    public void createMessage(String mode, int offset, char c);

    /***
     * Méthode qui permet d'envoyer un Message via un IChannel.
     * La méthode se termine quand le message est envoyé completement.
     * Pré-cond : le IChannel ne permet l'entrelacement des messages.
     * @param iChannel channel à travers lequel on envoie le Message.
     */
    public void sendMessage(IChannel iChannel);

    /***
     * Méthode qui permet de récupérer un Message via un IChannel.
     * La méthode ne se termine que lorsque le message a été reçu entièrement.
     * Pré-cond : le IChannel ne permet l'entrelacement des messages.
     * @param iChannel channel à travers lequel on récupère le Message.
     */
    public void receiveMessage(IChannel iChannel);

    /***
     * Méthode qui permet de récupérer les informations du message dans un format standardisé.
     * La taille du tableau est 1 si le mode est DEL, 2 si le mode est ADD.
     * @return les informations du message dans un String[]; avec [0] = offset, [1] = caractere à ajouter.
     */
    public String[] decodeMessage();
}
