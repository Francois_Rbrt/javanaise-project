/***
 * App class
 * Launch application wich use ProxyJvnObject
 * Contact:
 *
 * Authors: ROBERT François, SAULLE Laura
 */

package irc.with.proxy;

import api.jvn.JvnException;
import api.proxy.ProxyJvnObject;
import api.server.JvnServerImpl;

import javax.swing.*;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class App extends JFrame{
    private JvnServerImpl js;
    private JTabbedPane tabs;
    private JTextField varName;

    /**
     * Main method
     * Create a JVN object nammed App for representing the Chat application
     **/
    public static void main(String argv[]) throws UnsupportedLookAndFeelException {
        // Création de la partie graphique de l'application
        App app;
        app = new App();
        app.setVisible(true);
    }

    /**
     * Create JFrame for the application
     * @throws UnsupportedLookAndFeelException if the lookandfeel doesn't exist
     */
    public App() throws UnsupportedLookAndFeelException {
        super("App");

        //Application d'un look'n feel
        UIManager.setLookAndFeel( new NimbusLookAndFeel() );

        //Instanciation du serveur
        this.js = JvnServerImpl.jvnGetServer();

        //Paramètres d'affichage
        setSize(500,320);
        setLocationRelativeTo(null);

        // Creation de la toolbar
        JToolBar toolBar = new JToolBar();
        varName = new JTextField(15);
        varName.addActionListener(new addListener(this));
        toolBar.add(varName);
        JButton addButton = new JButton(" + ");
        addButton.addActionListener(new addListener(this));
        toolBar.add(addButton);
        toolBar.addSeparator();
        JButton supprButton = new JButton(" - ");
        supprButton.addActionListener(new removeListener(this));
        toolBar.add(supprButton);

        //Creation du conteneur d'onglets
        tabs = new JTabbedPane();
        tabs.setPreferredSize( new Dimension(545,201) );

        //Rassemblement des composants
        JPanel contentPane = (JPanel) getContentPane();
        contentPane.add(toolBar,BorderLayout.NORTH);
        contentPane.add(tabs);

        //Paramètres de fermeture de l'application
        addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e) {
                try {
                    js.jvnTerminate();
                    System.out.println("close");
                } catch (JvnException jvnException) {
                    jvnException.printStackTrace();
                }
            }
        });
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

    }

    /**
     * Internal class to manage user events (add a Sentence) on the application
     **/
    private class addListener implements ActionListener {
        private App app;

        public addListener(App app) {
            this.app = app;
        }

        /**
         * Management of user events
         **/
        @Override
        public void actionPerformed(ActionEvent e) {
            //Cration d'un onglet et son composant associé
            String name = app.varName.getText();
            Sentence sentence = (Sentence) ProxyJvnObject.newInstance(new SentenceImpl(),name);
            JPanel irc = new IrcProxy(sentence);
            app.tabs.addTab(name,irc);
            int index = app.tabs.indexOfComponent(irc);
            app.tabs.setSelectedIndex(index);
        }
    }

    /**
     * Internal class to manage user events (remove a Sentence) on the application
     **/
    private class removeListener implements ActionListener {
        private App app;

        public removeListener(App app) {
            this.app = app;
        }

        /**
         * Management of user events
         **/
        @Override
        public void actionPerformed(ActionEvent e) {
            //Fermeture d'un onglet et son composant associé
            int index = app.tabs.getSelectedIndex();
            app.tabs.remove(index);
        }
    }
}
