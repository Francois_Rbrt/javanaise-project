/***
 * JAVANAISE Implementation
 * ProxyJvnObject class
 * Contact:
 *
 * Authors: ROBERT François, SAULLE Laura
 */

package api.proxy;

import api.jvn.JvnException;
import api.jvn.JvnObject;
import api.server.JvnServerImpl;

import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class ProxyJvnObject implements InvocationHandler {

    private JvnObject jo;

    /**
     * ProxyJvnObject Constructor
     * Create proxy with object given
     * Create and register the object in coordinator, or look up object if it is already registered
     * @param obj object to intercept
     * @param jon name of the object
     */
    private ProxyJvnObject (Serializable obj, String jon) {
        try {
            JvnServerImpl js = JvnServerImpl.jvnGetServer();

            this.jo = js.jvnLookupObject(jon);
            if (jo == null) {
                jo = js.jvnCreateObject(obj);
                js.jvnRegisterObject(jon, jo);
                System.out.println("register done");
            }
            else {
                System.out.println("lookup done");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Create ProxyJvnObject
     * @param obj object to encapsulate
     * @param jon name of the object to communicate with coordinator
     * @return object encapsulated with proxy
     */
    public static Object newInstance(Serializable obj, String jon) {
        return java.lang.reflect.Proxy.newProxyInstance(
                obj.getClass().getClassLoader(),
                obj.getClass().getInterfaces(),
                new ProxyJvnObject(obj,jon));
    }

    /**
     * Call intercept object method when ProxyJvnObject is used
     * @param proxy object encapsulated with proxy
     * @param method method to call
     * @param args arguments of method
     * @return result of method called
     * @throws JvnException
     * @throws Exception
     */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws JvnException, Exception {
        try {
            if(method.isAnnotationPresent(Log.class)) {
                Object obj;
                Log.LogType logType = method.getAnnotation(Log.class).logtype();
                switch (logType) {
                    case READ:
                        jo.jvnLockRead();
                        //System.out.println("lock read");

                        break;
                    case WRITE:
                        jo.jvnLockWrite();
                        //System.out.println("lock write");
                        break;
                }
                obj = method.invoke(jo.jvnGetSharedObject(), args);
                //System.out.println("invocation of " + method.getName());
                jo.jvnUnLock();
                //System.out.println("unlock");
                return obj;
            }
        }
        catch (JvnException e) {
            throw new JvnException(e.getMessage());
        }
        return null;
    }
}
