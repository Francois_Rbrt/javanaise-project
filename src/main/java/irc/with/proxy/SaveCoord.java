package irc.with.proxy;

import api.coord.JvnCoordImpl;
import api.jvn.JvnException;

import java.rmi.RemoteException;

public class SaveCoord implements Runnable {
    JvnCoordImpl coord = null;
    int time=10000;

    public SaveCoord(JvnCoordImpl coord ,int i){
        this.coord=coord;
        time=i*1000;
    }
    @Override
    public void run() {
        do {
            try {
                Thread.sleep(time);//wait 10sec and save the state of the coordinator
                coord.coord_state_save();
                System.out.println("[SAVE] Save Done ...");
            } catch (InterruptedException | RemoteException e) {
                e.printStackTrace();
            } catch (JvnException e) {
                e.printStackTrace();
            }
        }while(true);
    }
}
