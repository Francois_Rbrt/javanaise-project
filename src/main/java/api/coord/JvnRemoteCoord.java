/***
 * JAVANAISE API
 * JvnRemoteCoord interface
 * This interface defines the remote interface provided by the Javanaise coordinator
 * Contact: 
 *
 * Authors: ROBERT François, SAULLE Laura
 */

package api.coord;

import api.jvn.JvnException;
import api.jvn.JvnObject;
import api.server.JvnRemoteServer;

import java.rmi.*;
import java.io.*;
import java.util.List;


/**
 * Remote Interface of the JVN Coordinator  
 */

public interface JvnRemoteCoord extends Remote {

    /**
     *  Allocate a NEW JVN object id (usually allocated to a
     *  newly created JVN object)
     * @throws RemoteException
     * @throws JvnException
     **/
    public int jvnGetObjectId()
            throws RemoteException, JvnException;

    /**
     * Associate a symbolic name with a JVN object
     * @param jon : the JVN object name
     * @param jo  : the JVN object
     * @param js  : the remote reference of the JVNServer
     * @throws JvnException
     * @throws RemoteException
     **/
    public void jvnRegisterObject(String jon, JvnObject jo, JvnRemoteServer js)
            throws java.rmi.RemoteException, api.jvn.JvnException;

    /**
     * Get the reference of a JVN object managed by a given JVN server
     * @param jon : the JVN object name
     * @param js : the remote reference of the JVNServer
     * @throws RemoteException
     * @throws JvnException
     **/
    public JvnObject jvnLookupObject(String jon, JvnRemoteServer js)
            throws RemoteException, JvnException;

    /**
     * Get a Read lock on a JVN object managed by a given JVN server
     * @param joi : the JVN object identification
     * @param js  : the remote reference of the server
     * @return the current JVN object state
     * @throws RemoteException
     * @throws JvnException
     **/
    public Serializable jvnLockRead(int joi, JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException;

    /**
     * Get a Write lock on a JVN object managed by a given JVN server
     * @param joi : the JVN object identification
     * @param js  : the remote reference of the server
     * @return the current JVN object state
     * @throws java.rmi.RemoteException
     * @throws JvnException
     **/
    public Serializable jvnLockWrite(int joi, JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException;

    /**
     * A JVN server terminates
     * @param js  : the remote reference of the server
     * @throws java.rmi.RemoteException
     * @throws JvnException
     **/
    public void jvnTerminate(JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException;

    /**
     * Remove an object of a remote server
     * @param joi id of the object
     * @param js the remote reference of the server
     * @throws RemoteException
     * @throws JvnException
     */
    public void jvnForget(int joi, Serializable obj, JvnRemoteServer js)
            throws java.rmi.RemoteException, JvnException;

    /**
     * save the state of the coordinator in a file
     * @throws java.rmi.RemoteException
     * @throws JvnException;
     */
    public void coord_state_save()throws RemoteException, JvnException ;

    /**
     * This function is called when there is a connexion problem with the coordinator
     * @throws java.rmi.RemoteException
     * @throws JvnException;
     */
    public void are_you_alive(List<Integer> lastUse,JvnRemoteServer js)throws RemoteException, JvnException ;
}


