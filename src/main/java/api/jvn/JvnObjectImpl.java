/***
 * JAVANAISE Implementation
 * JvnObjectImpl class
 * Contact :
 *
 * Authors : ROBERT François, SAULLE Laura
 */

package api.jvn;


import api.server.JvnServerImpl;

import java.io.Serializable;

public class JvnObjectImpl implements JvnObject {

    private final int id;
    private String lockValue;
    Serializable object;

    /**
     * JvnObject Constructor
     * @param id id of the object
     * @param object serializable
     */
    public JvnObjectImpl(int id, Serializable object) {
        super();
        this.id = id;
        this.lockValue = "NL";
        this.object = object;
    }

    /**
     * Get a Read lock on the shared object
     * @throws JvnException
     **/
    @Override
    public synchronized void jvnLockRead() throws JvnException {
        // Informer le serveur que l'objet va être utilisé
        update();

        //System.out.println("lock read " + lockValue);
        switch (lockValue) {
            case "RC" :
                lockValue = "R";
                break;
            case "WC" :
                lockValue = "RWC";
                break;
            case "NL" :
                // Appel au serveur pour demander un nouveau verrou
                Serializable state = JvnServerImpl.jvnGetServer().jvnLockRead(this.jvnGetObjectId());
                if(state != null)   // maj
                    object = state;
                lockValue = "R";
                break;
            default :
                throw new JvnException("Error: LockValue = " + lockValue + ", can't access to read.");
        }
    }

    /**
     * Get a Write lock on the object
     * @throws JvnException
     **/
    @Override
    public synchronized void jvnLockWrite() throws JvnException {
        // Informer le serveur que l'objet va être utilisé
        update();

        //System.out.print("lock write " + lockValue);
        switch (lockValue) {
            case "WC" :
                lockValue = "W";
                break;
            case "NL" :
            case "RC":
                // Appel au serveur pour demander un nouveau verrou
                Serializable state = JvnServerImpl.jvnGetServer().jvnLockWrite(this.jvnGetObjectId());
                if(state != null)   // maj
                    object = state;
                lockValue = "W";
                break;
            default :
                throw new JvnException("Error: LockValue = " + lockValue + ", can't access to write.");
        }
    }

    private synchronized void update() throws JvnException {
        int lockChanged = JvnServerImpl.jvnGetServer().updateData(this.jvnGetObjectId());

        switch (lockChanged) {
            case 0:
                lockValue = "NL";
                break;
            case -1:
                throw new JvnException("Error in data : no remaining object " + id);
            default:
                break;
        }
    }

    /**
     * Unlock  the object
     * @throws JvnException
     **/
    @Override
    public synchronized void jvnUnLock() throws JvnException {
        //System.out.print("unlock " + lockValue + " -> ");
        switch (lockValue) {
            case "R" :
                lockValue = "RC";
                break;
            case "W" :
            case "RWC" :
                lockValue = "WC";
                break;
            default :
                throw new JvnException("Error: LockValue = " + lockValue + ", can't unlock.");
        }
        //System.out.println(lockValue);
        notifyAll();
    }

    /**
     * Get the object identification
     **/
    @Override
    public int jvnGetObjectId() {
        return id;
    }

    /**
     * Get the shared object associated to this JvnObject
     **/
    @Override
    public Serializable jvnGetSharedObject() {
        return this.object;
    }

    /**
     * Invalidate the Read lock of the JVN object
     * @throws JvnException
     **/
    @Override
    public synchronized void jvnInvalidateReader() throws JvnException {
        try {
            while (lockValue.equals("R") || lockValue.equals("RWC")) {
                wait();
            }
            lockValue = "NL";
        }
        catch (InterruptedException e) {
            throw new JvnException(e.getMessage());
        }
    }

    /**
     * Invalidate the Write lock of the JVN object
     * @return the current JVN object state
     * @throws JvnException
     **/
    @Override
    public synchronized Serializable jvnInvalidateWriter() throws JvnException {
        try {
            while (lockValue.equals("W") || lockValue.equals("RWC")) {
                wait();
            }
            lockValue = "NL";
        }
        catch (InterruptedException e) {
            throw new JvnException(e.getMessage());
        }
        //System.out.println("objet renvoyé : " + object);
        return jvnGetSharedObject();
    }

    /**
     * Reduce the Write lock of the JVN object
     * @return the current JVN object state
     * @throws JvnException
     **/
    @Override
    public synchronized Serializable jvnInvalidateWriterForReader() throws JvnException {
        if (lockValue.equals("RWC") || lockValue.equals("WC")) { lockValue = "RC"; }
        else {
            try {
                while (lockValue.equals("W")) {
                    wait();
                }
                lockValue = "RC";
            }
            catch (InterruptedException e) {
                throw new JvnException(e.getMessage());
            }
        }
        //System.out.println("objet renvoyé : " + object);

        return jvnGetSharedObject();
    }
    /**
     * force the value of the object stored
     **/
    public void forceObjectValue(Serializable s){
        this.object = s;
    }
}
