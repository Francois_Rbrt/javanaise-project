On identifie 3 grosses partie du programme :
    L'application graphique qui va récupérer des évenements claviers et mettre le contenu des client à jour en fonctions des actions faites
    Les clients qui vont contenir un champ de saisi graphique, et un système de communication avec les autres clients via un serveur
    Le serveur qui va servir de passerelle entre tous les clients qui sont connectés.

En terme de thread :
- il y a un thread par client qui se charge  de la partie graphique : mise à jour de l'interface graphique en fonction des messages reçus par le client correspondant
- il y a pour chaque client un thread qui va se charger de lire en continue les messages qui viennent des autres clients, afin d'effectuer le traitement mentionné ci dessus
    Il y aura aussi : soit un thread "worker" qui attend de la part de l'application graphique des ordres (en réaction a un evenement clavier : ordre d'envoyer le message correspondant)
                      soit un thread lancé à la volée par l'appli graphique pour la réaction à un evenement : ce thread meurt après avoir envoyé le message au serveur, mais il y aura un "petit thread" comme celui la lancé à chaque évenement clavier.
- Pour le serveur : même principe : un thread par "connexion" qui va attendre de recevoir un message, faire le traitemet de celui ci, et s'il doit être broadcaster on fera intervenir le deuxième type de thread du serveur décrit ce dessous.
            Il peut y avoir comme dans le client soit un "worker" qui va attendre d'avoir des messages a broadcaster pour faire le travail demandé, soit un thread lancé à la volée qui s'occupera de brodcast un message donné.

Nous avons décidé aussi bien pour le client que le serveur d'implémenté la stratégie du thread écrivain "a la volée".
Le fait de lancer un thread écrivain lorsque l'on veut envoyer un message au serveur depuis un client ( à la réception d'un évenement clavier) permet de ne pas bloquer le thread graphique :
	L'écriture d'un message pouvant être bloquant on ne veut pas que l'application graphique soit bloquée le temps de l'envoie du message.
Le fait de lancer un thread écrivain côté serveur à la réception d'un message permet de ne pas bloquer le thread lecteur :
	si le thread lecteur se retrouve bloqué il ne vide plus son buffer de réception, si ce dernier se remplie ceci provoquera le blocage des threads écrivains côté client : le client se retrouverai dans l'incapacité de communiquer avec les autres clients.

##########################################################################################################

	Classe nécessaire :
	Classe principale : HackTextSample : fichier fourni + modification 
	Composant graphique "client" : doit contenir la zone de texte et permettre de traiter les évenements dans l'event queue
	Client 
	Serveur

##########################################################################################################
	HackTextSample :
	Contiendra la liste des "ClientComponent"
	Fonction main qui : 
		lance le serveur 
		lance les ClientComponent
		lance la vue 
	--------	
	ClientComponent :
	Composant graphique qui contient : 
		-une zone de texte 
		-un broker
		-un Client
		-une event queue
	Lors de sa création :
		-Initialise la zone de texte
		-Ajoute un "KeyListener" sur la zone de texte (passé en paramètre du constructeur mais pas stoqué)
		-Ajoute la zone de texte à la frame "parent" (passé en paramètre du constructeur mais pas stoqué)
		-Créer un client avec le port(passé en paramètre du constructeur mais pas stoqué) , le broker, et une référence vers le ClientComponent (this)
		-Lance le client dans un nouveau thread
		
	définition de processEvent(AWTEvent evt)  :
		-Appel sur la zone de texte en fonction de l'évenement reçu pour la mise à jour graphique
		
	-------
	Client
        - Broker
        - port
        - iChannel
        - composant graphique
    Lors de son lancement :
        se connecte au serveur grace au broker, sur le port donné, pour obtenir le ichannel.
        Lance un thread Client reader

	-------
	Client Reader
	    - Channel de communication
	Lors de son lancement :
	    Lit des messages sur le channel (appel qui peut être bloquant s'il n'y a pas de message à lire)
	    En fonction de ce qui est lu, ajoute sur l'EventQueue du composant graphique
	-------
	Client Writer
	    - Channel de communication
	    - Un message
	Lors de son lancement :
	    Envoie le message sur le channel (appel qui peut être bloquant)
	------
	Server
	    -Liste de port
	    -broker
	Lors de son lancement :
	    Accepte les connexions sur tout les ports qui sont dans la liste
	    Lance un thread ServerReader par connexion
	-------
	ServerReader
	    -Channel
	    -Liste des channels pour communiquer avec les clients
	Lors de son lancement :
	    Lit sur le channel (appel qui peut être bloquant)
	    Pour tout les autres channels avec lequel le serveur à une connexion : créer un thread ServerWriter pour broadcast le message qui à été reçu
	-------
	ServerWriter
	    - Channel de communication
	    - Un message
	Lors de son lancement :
	    Envoie le message sur le channel (appel qui peut être bloquant)
	
	
	
##########################################################################################################

La statégie du thread "worker" consisterait à executer l'algorithme suivant :
	tant que (vrai) faire :
	|	lire action dans un buffer d'action
	| 	executer l'action
	--	
	Si on est au niveau du serveur : Le buffer d'action mentionné ci dessus sera rempli par les threads lecteurs, qui mettront les messages à broadcaster aux autres clients
	Si on est au niveau du client : le buffer d'action sera rempli par le thread graphique à la réception d'un évenement clavier.
	
	
	#Cette stratégie serait intéressante si l'on veut faire évoluer notre implémentation : 
	* Actuellement côté client à chaque lettre écrite dans une champ de texte il y a un message envoyé, ce message contient le caractère à ajouter et son offset. 
	Il serait intéressant "d'empaquetter" plusieurs évenements rapproché dasn le temps, avec l'envoie d'un message qui contient donc une chaine de caractère au lieu de seulement un caractère.
	 
	* Côté serveur il y a un thread écrivain créé pour chaque message à transmettre à d'autres clients, pour chaque client, c'est à dire que si le serveur reçoit 1 message sur un port x, il créer un thread écrivain pour chaque client connecté au serveur qui se chargera de transmettre ce message.
	Il serait interessant d'avoir un thread worker qui a connaissance des messages qui sont transmis aux autres clients : ceci permettrait de régler des conflits qui ne sont pas géré actuellement par notre protocole. Par exemple si le client 1 (respectivement le client 2) veut ajouter le caractère 'a' (resp le caractère 'b' ) à l'offset 0, l'un aura en "local" le texte "ab" et l'autre le texte "ba".
	Avec un thread worker, on pourrait traiter ce genre de conflit au niveau du serveur en appliquant par exemple la politique suivante : si plusieurs messages consécutifs veulent agir sur le même offset, on "donne raison" au premier arrivé, et on modifie les offsets des autres messages avant de les broadcaster afin de maintenir un état cohérent chez tout le monde.
	Ceci permettrait d'améliorer les performances de notre protocole d'échange ainsi que de maintenir la cohérence entre les différents clients.
	
	
	Implémenter cette stratégie nécessiterait : 
		- d'implémenter un "format d'échange" entre l'application graphique et les clients via un buffer d'action.
		- d'implémenter le thread worker mentionné plus haut.
		- modifier le comportement des thread graphique lorsqu'ils réagissent à un évenement clavier (ne plus lancer de Thread ClientWritter) .
	Au vu du découpage de nos classes et des rôles de chaque type de thread, ceci est facilement faisable.
		
##########################################################################################################	

Une autre évolution de ce programme pourrait être de communiquer différement qu'avec un broker et des channels, par exemple une socket
Pour ce faire il faudrait modifier plusieurs choses dans notre code :
	Le serveur se comporterait de la même manière, il accepte les connexions des clients, lance un thread lecteur par connexion, il utilise un objet de type "serverSocket" au lieu du broker
	Le client doit avoir un objet de type "Socket", essaye de se connecter au serveur sur un port fourni grace à son @ip qui est fournie également.
	Au lieu d'écrire sur le Ichannel il écrirait sur le buffer d'envoie de sa socket, et il lirait sur le buffer de réception de la socket au lieu du ichannel.
	Il faut donc changer le programme principal, quelque classe pour remplacer le système de communication via le channel, mais globalement chaque thread garde le même rôle et "on ne casse pas tout" en changeant la méthode de communication.
	
##########################################################################################################

	Dans une architecture client serveur comme décrit ici pour une application de document partagé on voudrait pouvoir étendre les fonctionnalités de nos communications :
	Il faudrait que "l'état du texte" soit stocké au niveau du serveur : cela permettrait de faire de la reprise sur pane côté client.
	On pourrait aussi mettre en place un système de sauvegarde de l'état du texte pour permettre une reprise sur pane du serveur etc.
	Par manque de temps nous n'avons pas implémenté tout ceci et nous nous sommes arrêté au design décrit plus haut,
	mais nous avons essayé de faire l'effort de conception / documentation nécessaire pour que cela ne soit qu'une histoire de temps et non de tout casser et tout refaire






