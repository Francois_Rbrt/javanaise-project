/***
 * Sentence class : used for keeping the text exchanged between users
 * during a chat application
 * Contact: 
 *
 * Authors: 
 */

package irc.with.proxy;


import api.proxy.Log;

public class SentenceImpl implements Sentence, java.io.Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	String 	data;
  
	public SentenceImpl() {
		data = new String("");
	}

	public void write(String text) {
		data = text;
	}

	public String read() {
		return data;	
	}
}