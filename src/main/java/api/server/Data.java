/***
 * JAVANAISE Implementation
 * Data class
 * Manage data of server
 * Contact :
 *
 * Authors : ROBERT François, SAULLE Laura
 */

package api.server;

import api.jvn.JvnException;
import api.jvn.JvnObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;



public class Data {
    private static final int NB_OBJ_MAX = 3;
    private int nbObj;
    private List<Integer> lastUse;
    private HashMap<String,Integer> jvnObjectNameMap;
    private HashMap<Integer, JvnObject> jvnObjectIdMap;


    /**
     * Data Constructor
     */
    public Data() {
        nbObj = 0;
        lastUse = new ArrayList<>();
        jvnObjectNameMap = new HashMap<>();
        jvnObjectIdMap = new HashMap<>();
    }

    /**
     * Return true if name is associated to an id
     * @param name key whose presence in this data is to be tested if
     * @return true if name is associated with an id
     */
    public synchronized boolean contains(String name) {
        return lastUse.contains(jvnObjectNameMap.get(name));
    }

    /**
     * Private method to remove an element if needed
     * @throws JvnException
     */
    private synchronized void removeIfNeeded() throws JvnException {
        if (nbObj == NB_OBJ_MAX) {
            // Stratégie d'implémentation : l'élément qui a été utilisé le moins récement est enlevé
            int idLastUser = lastUse.get(0);
            JvnServerImpl.jvnGetServer().jvnForget(idLastUser,jvnObjectIdMap.get(idLastUser).jvnGetSharedObject());
            lastUse.remove(0);
            nbObj--;
        }
    }

    /**
     * Add an object in data with an id
     * @param id id of element
     * @param obj object to add
     * @throws JvnException
     */
    public synchronized void put(int id, JvnObject obj) throws JvnException {
        removeIfNeeded();
        jvnObjectIdMap.put(id,obj);
        lastUse.add(nbObj,id);
        nbObj++;
        System.out.println(lastUse);
    }

    /**
     * Add an object in data with a name id and id
     * @param name name of element
     * @param id id of element
     * @param obj object to add
     * @throws JvnException
     */
    public synchronized void put(String name, int id, JvnObject obj) throws JvnException {
        removeIfNeeded();
        if(!jvnObjectNameMap.containsKey(name)) {
            jvnObjectNameMap.put(name,id);
            jvnObjectIdMap.put(id,obj);
        }
        lastUse.add(nbObj,id);
        nbObj++;
        System.out.println(lastUse);
    }

    /**
     * Associate a name to an id (id must be already in data)
     * @param name name of element
     * @param id id of element
     */
    public synchronized void put(String name, int id) {
        jvnObjectNameMap.put(name,id);
    }

    /**
     * Return the id associated to a specific name
     * @param name the name whose associated with the id to return
     * @return id associated
     */
    public synchronized int getId(String name) {
        return jvnObjectNameMap.get(name);
    }

    /**
     * Return the object associated to a specific id
     * @param id of the value
     * @return the value to which the specified id is associated, or null if this datas contain no mapping for the id
     */
    public synchronized JvnObject getObject(int id) {
        return jvnObjectIdMap.get(id);
    }

    /**
     * Return the object associated to a specific id
     * @return the list of object ids that the server has
     */
    public synchronized List<Integer> getLastUse() {
        return lastUse;
    }


    /**
     * Refresh cache
     * @param id id of element used
     * @return state of changement (0 : lock remove, 1 : lock unchanged, -1 : error)
     */
    public synchronized int update(int id) {
        if (jvnObjectNameMap.containsValue(id)) {
            int index = -1;
            for (int i = 0; i < nbObj; i++) {
                if(lastUse.get(i) == id) {
                    index = i;
                }
            }
            if(index == -1){
                try {
                    removeIfNeeded();
                } catch (JvnException e) {
                    e.printStackTrace();
                }
                for (String name : jvnObjectNameMap.keySet()) {
                    if (jvnObjectNameMap.get(name) == id)  {
                        try {
                            JvnServerImpl.jvnGetServer().jvnLookupObject(name);
                            return 0; // NL
                        } catch (JvnException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
            else {
                lastUse.remove(index);
                lastUse.add(nbObj-1,id);
                System.out.println(lastUse);
                return 1; // NOT_CHANGED
            }
        }
        return -1; //NOT_REGISTERED
    }
    /**
     * this method is called to put up to date the data locally, after a coordinator crash
     * @param ser_list : the list of the objects states that have to be updated
     */
    public void clearCache(List<Serializable> ser_list)  {
        for(int i=0; i < ser_list.size();i++){
            //We unlock the object
            try {
                jvnObjectIdMap.get(lastUse.get(i)).jvnUnLock();
            } catch (JvnException e) {
                //there were no lock on the object, not a problem here
            }
            //we now force the value of this object to be what the coord stored in memory before crash :
            jvnObjectIdMap.get(lastUse.get(i)).forceObjectValue(ser_list.get(i));
        }
    }
}
