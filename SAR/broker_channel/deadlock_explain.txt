explication du deadlock :

le code du test fourni se comporte de la façon suivante : 
	Le programme lance un client qui : demande une connexion sur le port 80, puis va appeler la fonction echo pour des tailles de messages allant de 1 à maxlength.
	La fonction echo(i) consiste a envoyer i messages de 1 octet au serveur, puis attendre le retour des i messages puis a vérifier que ce qui a été reçu est égal à ce qui a été envoyé.
	
	Le programme lance aussi un serveur qui accepte des connexions sur le port 80, et renvoie à l'identique les messages qui ont été reçu.
	
	
	Le blocage apparait donc quand i > capacity*2 +10 ( le facteur limitant est le fait que le serveur lise au plus 10 octets en "une salve" ) car:
	le client envoie capacity octets, il se bloque car le buffer du serveur est plein
	le serveur lit 10 octets, reveille le client car il peut de nouveau recevoir des messages, et envoie 10 octets par 10 octets au client jusqu'a ce que son buffer soit vide, ou jusqu'a ce que le buffer du client soit plein
	de cette maniere on va finir par arriver a une situation ou le client est bloqué car le buffer du serveur est plein (capacity octets envoyés), le serveur a lu 10 octets, et il est bloqué car le client a son buffer rempli (capacity octets renvoyés).
	
	
	Pour corriger ca : il faudrait que dans la fonction echo : si le client se retrouve bloqué, que le serveur est bloqué, débloquer le client pour qu'il commence a lire d'une manière ou d'une autre, puis reprendre son envoie jusqu'a ce qu'il finisse d'envoyer tout ce qu'il voulait a la base.
	idée de correction : avant de faire un "write" dans la fonction "send" du client : regarder si le write va être la cause d'un blocage du thread client alord que le serveur est lui même bloqué :
		il faut donc que le client regarde l'état de son buffer avant d'envoyer quoi que ce soit, s'il est plein cela peut mener à la situation d'interblocage. On renvoie depuis la fonction send le nombre d'octets envoyé jusque la.
		dans le code de la fonction "echo" il faut donc : appeler la fonction send, si le nombre d'octets envoyé à la fin de l'appel à "send" est inférieur au nombre total voulu, on passe à l'appel de la fonction "rcv" pour vider son buffer local.
		On boucle jusqu'a ce que le client ait fini de tout envoyer et tout recevoir.
