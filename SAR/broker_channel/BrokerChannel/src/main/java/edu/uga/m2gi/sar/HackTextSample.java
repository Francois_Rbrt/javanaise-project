package main.java.edu.uga.m2gi.sar;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.TextArea;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;

public class HackTextSample {

  ClientComponent clients[];
  private static final Integer CAPACITY = 64;

  public static void main(String args[]) {
    Frame frame = new Frame();
    frame.addWindowListener(new _WindowListener());
    //creation du broker:
    IBrokerImpl broker=new IBrokerImpl(64);
    //lancement du serveur :
    ArrayList<Integer> port_list=new ArrayList<Integer>();
    for(int i=80;i<83;i++){
      port_list.add(i);
    }
    Thread serverThread =new Thread(new Server(broker, port_list,64));
    serverThread.start();

    //Lancement de la vue
    new HackTextSample(frame,broker);
    frame.setSize(600, 800);
    frame.pack();
    frame.setVisible(true);
  }


  HackTextSample(Frame frame,IBroker broker) {

    frame.setLayout(new FlowLayout());
    _KeyListener l = new _KeyListener();
    //Création des composants clients
    clients = new ClientComponent[3];
    for (int i = 0; i < clients.length; i++) {
      ClientComponent client = new ClientComponent(frame,l,broker,80+i,CAPACITY);
      clients[i] = client;
    }
  }

  class _KeyListener implements KeyListener {

    private boolean ignoredKey(int code) {
      return (code == KeyEvent.VK_LEFT) || (code == KeyEvent.VK_RIGHT)
          || (code == KeyEvent.VK_UP) || (code == KeyEvent.VK_DOWN)
          || (code == KeyEvent.VK_PAGE_UP) || (code == KeyEvent.VK_PAGE_DOWN)
          || (code == KeyEvent.VK_SHIFT);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
      TextArea src = (TextArea) e.getSource();
      int offset = src.getCaretPosition();
      //on récupère l'indice du client ou il y a eut un changement
      int index=0;
      for (int i = 0; i < clients.length; i++) {
        if (clients[i].text == src) {
          index=i;
        }
      }
      int code = e.getKeyCode();
      if (code == KeyEvent.VK_BACK_SPACE) {
        if (offset > 0) {
          //Creation du message
          Message newMessage=new MessageImpl();
          newMessage.createMessage("DEL",offset-1,'c');
          //creation du thread qui va l'envoyer
          new ClientWriter(clients[index].client.iChannel,newMessage);
        }
      } else if (code == KeyEvent.VK_DELETE) {
        String text = src.getText();
        if (offset < text.length()) {
          //Creation du message
          Message newMessage=new MessageImpl();
          newMessage.createMessage("DEL",offset,'c');
          //creation du thread qui va l'envoyer
          new ClientWriter(clients[index].client.iChannel,newMessage);
        }
      } else if (!ignoredKey(code)) {
        char[] chars = new char[1];
        chars[0] = e.getKeyChar();
        String s = new String(chars);
        //Creation du message
        Message newMessage=new MessageImpl();
        newMessage.createMessage("ADD",offset,chars[0]);
        //creation du thread qui va l'envoyer
        new ClientWriter(clients[index].client.iChannel,newMessage);
      }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }

  }

  static class _WindowListener implements WindowListener {

    @Override
    public void windowOpened(WindowEvent e) {
    }

    @Override
    public void windowClosing(WindowEvent e) {
      System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {
      System.exit(0);
    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {
    }

    @Override
    public void windowActivated(WindowEvent e) {
    }

    @Override
    public void windowDeactivated(WindowEvent e) {
    }

  };

}