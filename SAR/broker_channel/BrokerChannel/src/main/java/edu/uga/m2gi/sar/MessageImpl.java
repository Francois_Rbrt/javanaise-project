package main.java.edu.uga.m2gi.sar;

import java.io.UnsupportedEncodingException;

/* La classe MessageImpl est une classe d'implémentation de la classe Message.
 *
 * On peut créer un message avec un mode, un caractere et un offset,
 * l'envoyer ou le recevoir via un IChannel, ainsi que le décoder.
 * Les modes supportées sont : ADD et DEL. Ils sont expliqué plus précisément au niveau de "createMessage".
 * Les messages créés ici sont encodés en UTF-8 et font une taille MESSAGE_SIZE de 8 octets.
 */
public class MessageImpl implements Message {
    static final int MESSAGE_SIZE = 8;
    private byte[] buffer;

    public MessageImpl() {
        buffer = new byte[MESSAGE_SIZE];
    }

    /***
     * Méthode qui permet de créer un message selon le format suivant :
     * Si c'est un ajout de caractere : <offset_du_nouveau_char>;<char>
     * Si c'est une suppression : <offset_à_supprimer>
     * Le message est encodé en UTF-8
     * @param mode mode du message à envoyer. ADD pour ajouter un caractere à droite de l'offset,
     *            DEL pour en supprimer un à l'offset indiqué.
     * @param offset offset auquel faire la transformation.
     * @param c caractere à ajouter. Si le mode est DEL, il ne sera pas pris en compte.
     */
    public void createMessage(String mode, int offset, char c){
        String s = "";
        switch (mode) {
            case "ADD":
                s = offset + ";";
                //if(!Character.isAlphabetic(c)) { s += "\\"; }
                s += "" + c;
                break;
            case "DEL":
                s = "" + offset;
                break;
            default:
                System.out.println("Erreur : " + mode + " cas non pris en charge");
                break;
        }
        while (s.length()<MESSAGE_SIZE){
            s+="|";
        }
        for(int i=0;i<MESSAGE_SIZE;i++) {
            char car = s.charAt(i);
            buffer[i] = (byte)car;
        }
    }

    /***
     * Méthode qui permet d'envoyer un Message via un IChannel.
     * La méthode se termine quand le message est envoyé completement.
     * Pré-cond : le IChannel ne permet l'entrelacement des messages.
     * @param iChannel channel à travers lequel on envoie le Message.
     */
    public void sendMessage(IChannel iChannel){
        int nbSent = 0;
        while(nbSent < MESSAGE_SIZE){
            nbSent += iChannel.write(buffer,nbSent,MESSAGE_SIZE-nbSent);
        }
    }

    /***
     * Méthode qui permet de récupérer un Message via un IChannel.
     * La méthode ne se termine que lorsque le message a été reçu entièrement.
     * Pré-cond : le IChannel ne permet l'entrelacement des messages.
     * @param iChannel channel à travers lequel on récupère le Message.
     */
    public void receiveMessage(IChannel iChannel){
        int nbReceived = 0;
        while(nbReceived < MESSAGE_SIZE){
            nbReceived += iChannel.read(buffer,nbReceived,MESSAGE_SIZE-nbReceived);
        }
    }

    /***
     * Méthode qui permet de récupérer les informations du message dans un format standardisé.
     * La taille du tableau est 1 si le mode est DEL, 2 si le mode est ADD.
     * @return les informations du message dans un String[]; avec [0] = offset, [1] = caractere à ajouter.
     */
    public String[] decodeMessage(){
        String s = "";
        for(int i=0;i<MESSAGE_SIZE;i++) {
            char car = (char)buffer[i];
            if(i!=0 && car=='|' && (char)buffer[i-1]!='\\')
                break;
            s+=car;
        }
        String[] res = s.split(";");
        return res;
    }
}
