/***
 * JAVANAISE API
 * JvnLaunch application
 * This interface defines the remote interface provided by the Javanaise coordinator
 * Contact:
 *
 * Authors: ROBERT François, SAULLE Laura
 */

package api.coord;

import api.jvn.JvnException;
import irc.with.proxy.SaveCoord;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.Scanner;

public class JvnLaunch {

    /**
     * Main method to lanch coordinator
     * @param args
     */
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);
        JvnCoordImpl coord = null;

        try {
            coord = new JvnCoordImpl();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        //lancement du thread de sauvegarde
        Thread t = new Thread(new SaveCoord(coord,10));
        t.start();
        System.out.println("Le système est lancé");
        System.out.println("\"quit\" pour quitter");
        String cmd;
        do{
            cmd = scanner.nextLine();
        }while(!cmd.equals("quit"));

        //we put in the log file that we terminated normally
        try {
            LocalDate date= LocalDate.now();
            FileOutputStream fout = new FileOutputStream(System.getProperty("user.dir")+"/coord_log_"+date+".txt", false);
            fout.write(0);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
