/***
 * JAVANAISE API
 * Contact:
 *
 * Authors: ROBERT François, SAULLE Laura
 */

package api.proxy;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)

/**
 * Interface Log annotation.
 * Annotation to make proxy able to lock the object created
 */

public @interface Log {
    enum LogType {
        READ,
        WRITE
    }
    LogType logtype();
}