package edu.uga.m2gi.sar.test00;

import static org.junit.Assert.*;

import edu.uga.m2gi.sar.ArrayCircularBuffer;
import edu.uga.m2gi.sar.CircularBuffer;
import org.junit.Before;
import org.junit.Test;

public class Test_ArrayCircularBuffer {

	CircularBuffer buff;
	CircularBuffer lil_buff;
	
	@Before
	public void init() {
		buff= new ArrayCircularBuffer();
		lil_buff= new ArrayCircularBuffer(2);
	}
	
	@Test
	public void testPushAndPullOneByte_equality() {
		byte b= new Byte("1");
		buff.push(b);
		assertEquals(b, buff.pull());
	}

	@Test
	public void testFull() {
		byte a= new Byte("1");
		byte b= new Byte("2");
		lil_buff.push(a);
		lil_buff.push(b);
		assertEquals(true, lil_buff.full());
	}
	
	@Test
	public void testNotFull() {
		byte a= new Byte("1");
		byte b= new Byte("2");
		lil_buff.push(a);
		lil_buff.push(b);
		lil_buff.pull();
		assertEquals(false, lil_buff.full());
	}
	
	@Test
	public void testPushAndPullFewBytes_equality() {
		byte a= new Byte("1");
		byte b= new Byte("2");
		byte c= new Byte("3");
		buff.push(a);
		buff.push(b);
		buff.push(c);
		assertEquals(a, buff.pull());
	}
	
	@Test(expected = IllegalStateException.class)
	public void testPushInFullBuffer() {
		byte a= new Byte("1");
		lil_buff.push(a);
		lil_buff.push(a);
		lil_buff.push(a);
	}

	@Test(expected = IllegalStateException.class)
	public void testPullInEmptyBuffer() {
		buff.pull();
	}

	@Test
	public void testEmpty() {
		assertEquals(true, buff.empty());
	}
}

