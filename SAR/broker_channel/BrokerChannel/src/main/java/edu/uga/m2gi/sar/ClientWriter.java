package main.java.edu.uga.m2gi.sar;

/*
        Cette classe représente un thread écrivain côté client, il y en a un nouveau pour chaque message qu'il faut transmettre au serveur.
        Un thread écrivain va écrire le message donnée sur le Ichannel donné.
        Cette action est faite dans un thread car l'appel aux fonctions nécessaire peut être bloquant, et on veut empêcher que la vue
        soit bloqué.
         */
class ClientWriter implements Runnable {
    private IChannel channel;
    private Message message;

    public ClientWriter(IChannel ch,Message message){
        this.channel=ch;
        this.message=message;
        new Thread(this).start();
    }

    @Override
    public void run() {
        //on envoie le message
        this.message.sendMessage(channel);
    }
}
