package test.java.edu.uga.m2gi.sar.test00;


import main.java.edu.uga.m2gi.sar.IBroker;
import main.java.edu.uga.m2gi.sar.IBrokerImpl;

public class Test01 {
    public static void main(String args[]) throws Exception {

        /*
         * with these values, a deadlock is unavoidable,
         * reverse them:
         *
         *     int capacity = 512;
         *     int maxLength = 64;
         *
         * and the deadlock does not happen...
         * In fact, the deadlock happens as soon as
         * the maximum length sent is larger than twice
         * the capacity of the circular buffers.
         */
        int capacity = 4;
        final int maxLength = 18;

        final IBroker broker = new IBrokerImpl(capacity);

        for (int i = 0; i < 3; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ClientMult c = new ClientMult(0, 80, broker, maxLength);
                    c.run();
                }
            }, "client").start();

            new Thread(new Runnable() {
                @Override
                public void run() {
                    Server s = new Server(80, broker);
                    s.run();
                }
            }, "server").start();

        }
    }
}
