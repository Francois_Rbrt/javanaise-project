/***
 * JAVANAISE API
 * JvnRemoteServer interface
 * Defines the remote interface provided by a JVN server 
 * This interface is intended to be invoked by the Javanaise coordinator 
 * Contact: 
 *
 * Authors: ROBERT François, SAULLE Laura
 */

package api.server;

import api.jvn.JvnException;

import java.rmi.*;
import java.io.*;
import java.util.List;


/**
 * Remote interface of a JVN server (used by a remote JvnCoord) 
 */

public interface JvnRemoteServer extends Remote {
	    
	/**
	* Invalidate the Read lock of a JVN object 
	* @param joi : the JVN object id
	* @throws RemoteException
	 * @throws JvnException
	**/
  public void jvnInvalidateReader(int joi)
	throws RemoteException, JvnException;
	    
	/**
	* Invalidate the Write lock of a JVN object 
	* @param joi : the JVN object id
	* @return the current JVN object state 
	* @throws RemoteException
	 * @throws JvnException
	**/
        public Serializable jvnInvalidateWriter(int joi)
	throws RemoteException, JvnException;
	
	/**
	* Reduce the Write lock of a JVN object 
	* @param joi : the JVN object id
	* @return the current JVN object state
	* @throws RemoteException
	 * @throws JvnException
	**/
   public Serializable jvnInvalidateWriterForReader(int joi)
	 throws RemoteException, JvnException;


	/**
	 * This function is called every time there is a remote exception in order to handle a coordinator crash
	 * @throws java.rmi.RemoteException
	 **/
	public void remoteExceptionHandler()
			throws RemoteException;

	/**
	 * this method is called when the coordinator restart after a crash, to update all the servers state to maintain coherence
	 * @param ser_list : the list of state of objects the server will have to put up to date in his cache
	 * @throws java.rmi.RemoteException
	 **/
    public void clearCache(List<Serializable> ser_list)
			throws RemoteException;
}

 
